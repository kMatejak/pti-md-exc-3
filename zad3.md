---
author: Krzysztof S. Matejak
title: Discovering Norway
subtitle: Svalbard
date: 24 X 2020
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
    \DeclareUnicodeCharacter{2248}{≈}
---


## Svalbard  

**Svalbard** is a group of islands located between the Arctic Ocean, Barents Sea, Greenland Sea, and the Norwegian Sea. The area is sometimes referred to as Spitsbergen, the main island with all the settlements. The islands are directly north of Norway, and under Norwegian rule since 1920. Svalbard's settlements are the northernmost permanently inhabited spots on the planet, after the Canadian military base at Alert.  
  
Svalbard is a unique place because of their nature, the extreme northern location and their legal status as neutral Norwegian territory. There is virtually no infrastructure outside a handful of small settlements. Svalbard has midnight sun from late April to late August, while winter darkness (polar night) lasts from late October to February. Because of the delicate nature and polar bear hazard movement outside settlements is strictly regulated.
  
## Towns  
  
All settlements in Svalbard are located on the main island.
  
1. Barentsburg – sole remaining Russian settlement, population 400  
2. Hornsund Polish Polar Station – Polish research station, population 10 in winter, around 20–30 in summer  
3. Longyearbyen – the "capital" and main Norwegian settlement with a population of 2100  
4. Ny-Ålesund – the most northerly civilian settlement in the world, population under 100  
5. Sveagruva – population 210  
  
![](./img/sva_towns_map.png){height=25%}  
  
## Understand  
  
Svalbard is the northernmost tip of Europe and, a few military bases aside, its settlements are the northernmost permanently inhabited spots on the planet.  
  
While Svalbard has never had an indigenous population, the islands are inhabited today by less than 3,000 people, nearly all of them in the main settlements of Longyearbyen and Barentsburg on Spitsbergen.  
  
All man-made objects older than 1946 are protected by law and can not be touched by visitors. Human graves are often shallow and human remains are in some cases visible on the ground. Remains of animals such as whales and polar bears of any age are also protected.  
  
## Map of Svalbard  
  
![Map of Svalbard](./img/topographic_map_of_Svalbard_1024px.png)  
  
## Costs  
  
Svalbard is by most measures **very expensive**: on Svalbard most things costs even more than mainland Norway. Accommodation in cheap guesthouses costs around 500 kr/night and sit-down meals are closer to 100 kr each, and both can easily be double if you want to stay in a full-service hotel. Guided activities start at about 500 kr per day (e.g., trekking and kayaking) but can go to 1000 kr and above for tours requiring specialist equipment.  
  
### Exchange rates for Norwegian kroner
  
| As of August 2018 |  |  |  
| ----: | :---: | :------ |  
| US$1  | $\approx$     | 8.3kr   |  
| €1    | $\approx$     | 9.5kr   |  
| UK£1  | $\approx$     | 10.6kr  |  
  
Exchange rates fluctuate. Current rates for these and other currencies are available from [XE.com](xe.com)  
  
## Climate  
  
Svalbard literally means "cold edge", an apt name for this northern land. The climate is Arctic, tempered by warm North Atlantic Current. Summers are cool (July average 6.1°C) and winters are cold (January average -15.8°C), but **wind chill** means that it usually feels colder. The North Atlantic Current flows along west and north coasts of Spitsbergen, keeping water open and navigable most of the year. The peak travel season for boating and cruise is during Svalbard's brief summer, from June to August, when it's light and not too cold outside. The "light winter" period (March-May) is the high season for fly and stay guests, when there is both sunlight and snow, is also increasingly popular for winter sports.  
  
Svalbard features the **[midnight sun](https://en.wikivoyage.org/wiki/Midnight_sun)** from 20 April to 23 August. Conversely, the sun stays under the horizon during the polar night from 26 October to 15 February. In northern Norway there is twilight mid-day during the **polar night**, but on Svalbard it is fully dark at least all of December.
  
## Islands of The Arctic Ocean  
  
![Map of Arctic Islands](./img/map_of_arctic_islands.png)  
  